/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionfacci;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class GestionFACCI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //instanciar las clases
        //enviar los datos por medio de los objetos
        //Mostrar los resultados
        
        PersonalServicio Pericles = new PersonalServicio();
        //Clase Persona es la clase padre (base) de Empleado
        //Propiedades Generales heredadas de la clase Persona
        Pericles.setNombres("Pericles");
        Pericles.setApellidos("Garcia");
        Pericles.setCedula("1112223334");
        Pericles.setEstadoCivil("Soltero");
        
        //Clase Empleado es la clase padre (base) de PersonalServicio
        //Clase Empleado es clase hija (derivada) de Persona
        //Propiedades heredadas de la clase Empleado
        Pericles.setNumeroDespacho(100);
        Pericles.setAnioEntrada(2000);
        
        //Clase PersonalServicio es clase hija (derivada) de Empleado
        //Seccion es la propiedad especializada
        Pericles.setSeccion("Planta Baja");
        
        Pericles.CambiarDespacho(200);
        Pericles.CambioEstadoCivil("Unido");
        Pericles.TrasladoSeccion("Tercer Piso");
        
       // System.out.println( Pericles.Imprimir());
        
        /*
        System.out.println(Pericles.getEstadoCivil());
        System.out.println(Pericles.getNumeroDespacho());
        System.out.println(Pericles.getSeccion());
        */
        
       
        
       // Persona miPersona = new Persona();
        
        Estudiante miEstudiante = new Estudiante();
        miEstudiante.setNombres("Jose Luis");
        miEstudiante.setApellidos("Perez");
        miEstudiante.setCurso("Cuarto Nivel");
        miEstudiante.MatricularCurso("Quinto Nivel");
       // System.out.println(miEstudiante.Imprimir());
        
        Empleado miEmpleado = new Empleado();
        miEmpleado.setNombres("Dimas");
        miEmpleado.setApellidos("Zambrano");
        miEmpleado.setNumeroDespacho(300);
        miEmpleado.CambiarDespacho(400);
       // System.out.println(miEmpleado.Imprimir());
        
        
        Profesor miProfesor = new Profesor();
        miProfesor.setNombres("Dimas");
        miProfesor.setApellidos("Zambrano");
        miProfesor.setDepartamento("Investigacion");
        miProfesor.CambioDepartamento("Vinculacion");
        //System.out.println(miProfesor.Imprimir());
        
        List<Persona> listaPersonalFACCI = new ArrayList<>();
        listaPersonalFACCI.add(Pericles);
        listaPersonalFACCI.add(miEmpleado);
        listaPersonalFACCI.add(miEstudiante);
        listaPersonalFACCI.add(miProfesor);
        
        for (Persona persona : listaPersonalFACCI) {
            System.out.println( persona.Imprimir());
        }
        
        
        
    }
    
}
