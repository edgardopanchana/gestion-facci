/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionfacci;

/**
 *
 * @author USUARIO
 */
public abstract class Persona {
    
    private String Nombres;
    private String Apellidos;
    private String Cedula;
    private String EstadoCivil;

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getEstadoCivil() {
        return EstadoCivil;
    }

    public void setEstadoCivil(String EstadoCivil) {
        this.EstadoCivil = EstadoCivil;
    }
    
    public void CambioEstadoCivil(String EstadoCivil)
    {
        this.setEstadoCivil(EstadoCivil);
    }
    
    public abstract String Imprimir();
    
    
}
