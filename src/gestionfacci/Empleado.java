/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionfacci;

/**
 *
 * @author USUARIO
 */
public class Empleado extends Persona{
    
    private int AnioEntrada;
    private int NumeroDespacho;

    public int getAnioEntrada() {
        return AnioEntrada;
    }

    public void setAnioEntrada(int AnioEntrada) {
        this.AnioEntrada = AnioEntrada;
    }

    public int getNumeroDespacho() {
        return NumeroDespacho;
    }

    public void setNumeroDespacho(int NumeroDespacho) {
        this.NumeroDespacho = NumeroDespacho;
    }
    
    public void CambiarDespacho(int NumeroDespacho){
        this.setNumeroDespacho(NumeroDespacho);
    }

    @Override
    public String Imprimir() {
    return this.getNombres() + " " + this.getApellidos() +
            " " + this.getNumeroDespacho();
    }
    
    
}
